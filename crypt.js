const bcrypt = require('bcrypt');



function hash(data){
  console.log("hashing data");
  return  bcrypt.hashSync(data,10); //crea el hash con el encroptado. 10 es el número de veces que se "ejecuta"

}

  //passwordFromUserInPlainText texto que manda el usuario desde la pantalla
  //passwordFromDBHashed pass guardada en bbdd.
  //La función internaente lo comprueba
  function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed){
  console.log("checking Password");

  //returm booleana
  return  bcrypt.compareSync(passwordFromUserInPlainText,passwordFromDBHashed);

}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
