console.log("TechUBank - Arrancando la consola nodeJS");

require('dotenv').config(); //con esto cargamos el fichero .env y recupera así las varieables globales. Lo añade en el objeto "process.env"
const express = require('express'); //cargamos en esta constante el módulo de express Porque así lo define el frameworf
const expressJwt=require('express-jwt');
const app = express(); //inicializar el framework de express
const port= process.env.PORT || 3000; //inicializamos el puerto si no hay otro puerto por defecto escuchamos en el 3000 (PORT es una variable que tienes definida sino coge el 3000)

const emailController = require('./controllers/mailController');
const usuarioController = require('./controllers/UsuarioController');
const loginController = require('./controllers/LoginController');
const cuentaController = require('./controllers/CuentaController');
const movimientoController = require('./controllers/MovimientoController');
const cambioController = require('./controllers/CambioController');


app.use(express.json());//para poder utilizar json, se actualiza esta variable. hace el json.parse internaente.

//esto es para q el CORS del front pueda ejectar la petición.
var enableCORS = function(req, res, next){
  // por esta función van a pasar todas las peticiones desde el front.
  //esto va a añadir a las cabeceras un tema para que valide que la petición es correcta.
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
//para permitir incluit nuestra cabecera que viene del front. Si queremos usar otro api se pone aqií
// añadir el parametro authorization donde va el token
  res.set("Access-Control-Allow-Headers", "Content-Type,authorization");

  next();

}// end enableCORS

app.use(enableCORS);

app.listen(port); //inicializa el servidor en el puerto indicado

console.log("API escuchando en el puerto BIP BIP BIP "+ port);

//login
app.post("/techUProyecto/login",loginController.login);
//logout
app.post("/techUProyecto/logout/:idUsuario",loginController.logout);

//obtener todos los usuarios
app.get("/techUProyecto/users",usuarioController.getUsuario);
//obtener usuario por ID
app.get("/techUProyecto/users/:idUsuario",usuarioController.getUsuarioPorId);
//crear usuario
app.post("/techUProyecto/users", usuarioController.crearUsuario);
//modificar datos usuario
app.put("/techUProyecto/users/:idUsuario", usuarioController.modificarUsuario);

app.get("/techUProyecto/cuentas/total/:idUsuario", cuentaController.getTotalCuentas);

//crear cuenta del usuario
app.post("/techUProyecto/cuentas/:idUsuario", cuentaController.crearCuenta);
//obtener cuentas del usuario
app.get("/techUProyecto/cuentas/:idUsuario", cuentaController.getCuentasUsuario);
//elimnar cuenta IBAN
app.delete("/techUProyecto/cuentas/:idUsuario/:IBAN", cuentaController.deleteCuenta);


//crear movimiento en cunta del usuario
app.post("/techUProyecto/movimientos/:idUsuario", movimientoController.altaMovimiento);

//obtener movimientos de una cuenta de un usuario
//ejemplo llamada localhost:3000/techUProyecto/movimientos/ES23-5252-8512-11-2180012771
app.get("/techUProyecto/movimientos/cuenta/:IBAN/:idUsuario",movimientoController.getMovimientosCuenta);

app.get("/techUProyecto/movimientos/:idUsuario",cuentaController.getMovimientosUsuario);

app.get("/techUProyecto/totales/:idUsuario",cuentaController.getBalanceMesUsuario);


//envio correos
app.post('/techUProyecto/email', emailController.envioMail);
app.get('/techUProyecto/password/:mail', usuarioController.getPassword);

//cambio
app.get('/techUProyecto/cambio/:monedaOrigen/:monedaDestino/:importe', cambioController.getCambio);
app.get('/techUProyecto/cambio/:monedaOrigen', cambioController.getCambioEuro);
