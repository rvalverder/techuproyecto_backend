const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
const url= 'http://localhost:3000';

var agent = chai.request.agent(url);


var should = chai.should(); //metodo para hacer la asercion, es decir, pasar el test unitario

describe("Test Usuarios",
  it("obtener datos de usuario", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      agent // se define dominio base donde hacer las peticiones
      .post('/techUProyecto/login')  // se añade el path
      .send({"email":"raul@mail.org", "password": "uno"})
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("logado "+res.body.idUsuario);
          //console.log(res);
          //console.log(err);
          res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
          res.body.should.have.property('token');
          res.body.should.have.property('idUsuario');
          res.body.msg.should.be.eql("Usuario logado con éxito");
          return agent.get("/techUProyecto/users/"+res.body.idUsuario)
           .set('authorization', res.body.token)
          .end(   //con la respuesta ve a este función
            function(err, res){
              console.log("datos usuario "+res.body.idUsuario);
              // console.log(res);
              // console.log(err);
              res.should.have.status(200);
              res.body.should.have.property("nombre"); // comprobamos la existencia de la propiedad no el valor
              res.body.should.have.property("apellido1");
              res.body.should.have.property("apellido2");
              res.body.should.have.property("email");
              res.body.should.have.property("password");
            //  res.body.msg.should.be.eql("usuario deslogado satisfactoriamente");
              done();
            });
          done();

        }//function (err,res)
      );//end

    }//funciton (done)
  )// it



)//describe
