const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
const url= 'http://localhost:3000';

var agent = chai.request.agent(url);


var should = chai.should(); //metodo para hacer la asercion, es decir, pasar el test unitario

describe("Test Login/logout",
  function (){
    it("Prueba recuperar usuarios", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      agent // se define dominio base donde hacer las peticiones
      .get('/techUProyecto/users')  // se añade el path
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("test dummy");

          res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
          res.body.should.be.a("array");
          for (user of res.body)
          {
            user.should.have.property("nombre"); // comprobamos la existencia de la propiedad no el valor
            user.should.have.property("apellido1");
            user.should.have.property("apellido2");
            user.should.have.property("email");
            user.should.have.property("password");
          }//for
          done(); // indica al framework de aserciones cuando puede comprobar las aserciones
        }//function (err,res)
      )//end
    }//funciton (done)
    )//it "Prueba recuperar usuarios"
    ,

    it("login", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
        agent // se define dominio base donde hacer las peticiones
        .post('/techUProyecto/login')  // se añade el path
        .send({"email":"sandra.fb@bbva.com", "password": "uno"})
        .end(   //con la respuesta ve a este función
          function(err, res){
            console.log("login procesado");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
            res.body.should.have.property('token');
            res.body.should.have.property('idUsuario');
            res.body.msg.should.be.eql("Usuario logado con éxito");
            //return agent.get()
            done(); // indica al framework de aserciones cuando puede comprobar las aserciones
          }//function (err,res)

        )//end
      }//funciton (done)

    )// it Prueba que la  API devuelve lista de usuarios correctos
  //}//function

  ,

  it("login erroneo usuario no existe", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      agent // se define dominio base donde hacer las peticiones
      .post('/techUProyecto/login')  // se añade el path
      .send({"email":"sandrass@mail.org", "password": "uno"})
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("login erroneo usuario no existe");
          res.should.have.status(404);  //200 es el codigo que devuelve cuando ha ido ok

          res.body.msg.should.be.eql("usuario / contraseña incorrecto");
          done(); // indica al framework de aserciones cuando puede comprobar las aserciones
        }//function (err,res)
      )//end
    }//funciton (done)
  )// it Prueba que la  API devuelve lista de usuarios correctos
,

  it("login erroneo password incorrecta", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      agent // se define dominio base donde hacer las peticiones
      .post('/techUProyecto/login')  // se añade el path
      .send({"email":"sandra.fb@bbva.com", "password": "unoss"})
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("login erroneo password incorrecta");
          res.should.have.status(401);  //200 es el codigo que devuelve cuando ha ido ok

          res.body.msg.should.be.eql("usuario / contraseña incorrecto");
          done(); // indica al framework de aserciones cuando puede comprobar las aserciones
        }//function (err,res)
      )//end
    }//funciton (done)
  )// it Prueba que la  API devuelve lista de usuarios correctos
,
  it("logout", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      agent // se define dominio base donde hacer las peticiones
      .post('/techUProyecto/login')  // se añade el path
      .send({"email":"sandra.fb@bbva.com", "password": "uno"})
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("logado "+res.body.idUsuario);
          //console.log(res);
          //console.log(err);
          res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
          res.body.should.have.property('token');
          res.body.should.have.property('idUsuario');
          res.body.msg.should.be.eql("Usuario logado con éxito");
          return agent.post("/techUProyecto/logout/"+res.body.idUsuario)
           .set('authorization', res.body.token)
          .end(   //con la respuesta ve a este función
            function(err, res){
              console.log("deslogado");
              // console.log(res);
              // console.log(err);
              res.should.have.status(200);
              res.body.msg.should.be.eql("usuario deslogado satisfactoriamente");
              done();
            });
          done();

        }//function (err,res)
      );//end

    }//funciton (done)
  )// it Prueba que la  API devuelve lista de usuarios correctos
  ,
    it("logout con  token incorrecto", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
        agent // se define dominio base donde hacer las peticiones
        .post('/techUProyecto/login')  // se añade el path
        .send({"email":"sandra.fb@bbva.com", "password": "uno"})
        .end(   //con la respuesta ve a este función
          function(err, res){
            console.log("logado "+res.body.idUsuario);
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
            res.body.should.have.property('token');
            res.body.should.have.property('idUsuario');
            res.body.msg.should.be.eql("Usuario logado con éxito");
            return agent.post("/techUProyecto/logout/"+res.body.idUsuario)
             .set('authorization', 'error')
            .end(   //con la respuesta ve a este función
              function(err, res){
                console.log("error token");
                // console.log(res);
                // console.log(err);
                res.should.have.status(401);
                res.body.msg.should.be.eql("Token inválido");
                done();
              });
            done();

          }//function (err,res)
        );//end

      }//funciton (done)
    )// it Prueba que la  API devuelve lista de usuarios correctos
    ,
      it("logout sin token", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
          agent // se define dominio base donde hacer las peticiones
          .post('/techUProyecto/login')  // se añade el path
          .send({"email":"sandra.fb@bbva.com", "password": "uno"})
          .end(   //con la respuesta ve a este función
            function(err, res){
              console.log("logado "+res.body.idUsuario);
              //console.log(res);
              //console.log(err);
              res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
              res.body.should.have.property('token');
              res.body.should.have.property('idUsuario');
              res.body.msg.should.be.eql("Usuario logado con éxito");
              return agent.post("/techUProyecto/logout/"+res.body.idUsuario)
               //.set('authorization', res.body.token)
              .end(   //con la respuesta ve a este función
                function(err, res){
                  console.log("no hay token");
                  // console.log(res);
                  // console.log(err);
                  res.should.have.status(401);
                  res.body.msg.should.be.eql("Es necesario el token de autenticación");
                  done();
                });
              done();

            }//function (err,res)
          );//end

        }//funciton (done)
      )// it Prueba que la  API devuelve lista de usuarios correctos
      ,
        it("logout token no coincide con usuario", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
            agent // se define dominio base donde hacer las peticiones
            .post('/techUProyecto/login')  // se añade el path
            .send({"email":"sandra.fb@bbva.com", "password": "uno"})
            .end(   //con la respuesta ve a este función
              function(err, res){
                console.log("logado "+res.body.idUsuario);
                //console.log(res);
                //console.log(err);
                res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
                res.body.should.have.property('token');
                res.body.should.have.property('idUsuario');
                res.body.msg.should.be.eql("Usuario logado con éxito");
                return agent.post("/techUProyecto/logout/666666")
                .set('authorization', res.body.token)
                .end(   //con la respuesta ve a este función
                  function(err, res){
                    console.log("token distinto de usuario");
                    // console.log(res);
                    // console.log(err);
                    res.should.have.status(403);
                    res.body.msg.should.be.eql("acceso prohibido");
                    done();
                  });
                done();

              }//function (err,res)
            );//end

          }//funciton (done)
        )// it Prueba que la  API devuelve lista de usuarios correctos

}//function
)//describe
