const requestJson = require ('request-json');
const cuentaController = require('./CuentaController');
const tokenController = require('./TokenController');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";
const baseMLabURL_runCommand = "https://api.mlab.com/api/1/databases/apitechusfb11ed/runCommand";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js

function crearMovimiento(req, res,idMovimiento)
{
  console.log("crearMovimiento");
  var fecha = new Date();
  //var options = { day: 'numeric', month: 'long',year: 'numeric' };

  var dia = fecha.getDate();
  var mes = fecha.getMonth()+1;
  var anno = fecha.getUTCFullYear();
  mes = mes <10 ? "0"+mes:mes;
  dia = dia <10 ? "0"+dia:dia;

  var fechaMostrar = dia+"/"+mes+"/"+anno;
  //console.log(fechaMostrar);//"20/12/2012"

  var nuevoMovimiento = {
    "idMovimiento"  :idMovimiento,
    "IBAN":req.body.IBAN,
    "importe":req.body.importe,
    "tipoMovimiento":req.body.tipoMovimiento,
    "concepto":req.body.concepto,
    "fechaMostrar": fechaMostrar,
    "fecha": { "$date" : fecha }
  }
  httpClient = requestJson.createClient(baseMLabURL);
  // insert movimiento
  httpClient.post("movimientos?" + mLabAPIKey, nuevoMovimiento,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({"msg":"Error insertando movimiento"});
      } else {
        console.log("movimiento creado");
        //actualiza balance en cuentas
        cuentaController.actualizaBalance (req, res,nuevoMovimiento.IBAN,nuevoMovimiento.importe);
      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//crearMovimiento


function altaMovimiento(req, res)
{
  console.log("----------------------");
  console.log("POST /techUProyecto/movimientos/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "s={'idMovimiento':-1}&l=1";//s con -1 ordena descendiente y l selecciona solo uno
    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"movimientos?"+ query +"&" + mLabAPIKey)

    httpClient = requestJson.createClient(baseMLabURL);
    //select max idMovimiento para obtener el idMovimiento para el nuevo movimiento
    httpClient.get("movimientos?"+ query +"&"  + mLabAPIKey,  //ojo con el nombre de la coleccion
      function(err,resMLab,body){
        if (err){
          res.status(500).send({"msg":"Error obteniendo id maxima"});
        } else {
          var idMovimiento = body[0] ? body[0].idMovimiento+1 : 1;
          crearMovimiento(req,res,idMovimiento);
        }
      }//function(err,resMLab,body)
    )//httpClient.get
  } else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//altaMovimiento

function getMovimientosCuenta (req, res){
  console.log("get /techUProyecto/movimientos/cuenta/:IBAN/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var IBAN = req.params.IBAN;
    var query = "q={'IBAN':'"+IBAN+"'}";

    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"movimientos?"+ query +"&" + mLabAPIKey)

    var httpClient = requestJson.createClient(baseMLabURL);
    //select cuentas where idUsuario e IBAN
    httpClient.get("movimientos?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          var response ={ "msg" : "Error obteniendo movimientos"};
          res.status(500);
        } else {
          if (body.length >0){
            var response = body;
          } else {
            var response = {"msg" : "la cuenta "+IBAN+" no tienen movimientos"}
            res.status(404);
          }
        }
        res.send(response);
      }//function (err,resMLab, body)
    );//  httpClient.get
  } else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//getMovimientosCuenta

function getMovimientosUsuario (req, res,qMovimientos){
  // console.log("La consulta es "+qMovimientos);
  // console.log("url completa "+baseMLabURL+"movimientos?"+ qMovimientos +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  //select cuentas where idUsuario e IBAN
  httpClient.get("movimientos?"+ qMovimientos +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
        var response ={ "msg" : "Error obteniendo movimientos"};
        res.status(500);
      } else {
        if (body.length >0){
          var response = body;
        } else {
          var response = {"msg" : "la cuenta "+IBAN+" no tienen movimientos"}
          res.status(404);
        }
      }
      res.send(response);
    }//function (err,resMLab, body)
  );//  httpClient.get
}//getMovimientosCuenta

function getBalanceMesUsuario (req, res,arrayCuentas){
  console.log("getBalanceMesUsuario");
  var runCommand = {

      "aggregate": "movimientos",
      "pipeline": [

      	   {"$project":
            {
             "year": { "$year": "$fecha" },
              "month": { "$month": "$fecha" },
              "day": { "$dayOfMonth": "$fecha" },
              "hour": { "$hour": "$fecha" },
              "minutes": { "$minute": "$fecha" },
              "seconds": { "$second": "$fecha" },
              "milliseconds": { "$millisecond": "$fecha" },
              "dayOfYear": { "$dayOfYear": "$fecha" },
              "dayOfWeek": { "$dayOfWeek": "$fecha" },
              "week": { "$week": "$fecha" },
              "importe": "$importe",
              "IBAN":"$IBAN"
            }
           },
      	 {"$match" :{ "IBAN": {"$in": arrayCuentas}}},


        { "$group": { "_id": "$month", "total": { "$sum" : "$importe" } } }
      ],
       "cursor": { }
   }

  //  console.log(runCommand);
  //  console.log(runCommand.pipeline);

  //  console.log("url "+baseMLabURL_runCommand+"runCommand?" + mLabAPIKey);
   var httpClient = requestJson.createClient(baseMLabURL_runCommand);

   httpClient.post("runCommand?" + mLabAPIKey, runCommand,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({ "msg" : "Error obteniendo total meses"});
      } else {
        // console.log("body con el acumulado de los meses para las cuentas del usuario seleccionado ");
        // console.log(body);
        var salida = body.cursor.firstBatch;
        salida.sort(GetSortOrder("_id"));
        res.status(201).send(salida);
      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//getMovimientosCuenta


//Comparer Function para ordenar array de json
function GetSortOrder(prop) {
    return function(a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}//GetSortOrder


function eliminarMovimientos(res,req){
  var IBAN = req.params.IBAN;

  var queryIBAN = "q={'IBAN':'"+IBAN+"'}";
  // console.log("La consulta es "+queryIBAN);
  // console.log("url completa "+baseMLabURL+"movimientos?"+ queryIBAN +"&" + mLabAPIKey)

  httpClient = requestJson.createClient(baseMLabURL);
  var deleteCuenta  = [];
  httpClient.put("movimientos?"+ queryIBAN +"&"  + mLabAPIKey, deleteCuenta, //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({ "msg" : "Error buscando IBAN"});
      } else {
          res.status(200).send({"msg" : "IBAN "+IBAN+" eliminado correctamente" })
      }
    })

}

module.exports.altaMovimiento = altaMovimiento;
module.exports.getMovimientosCuenta=getMovimientosCuenta;
module.exports.getMovimientosUsuario=getMovimientosUsuario;
module.exports.getBalanceMesUsuario=getBalanceMesUsuario
module.exports.eliminarMovimientos = eliminarMovimientos;
