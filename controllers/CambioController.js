const requestJson = require ('request-json');

const baseCambioURL = "https://api.cambio.today/v1/"

const cambioKey = "key=" + process.env.CAMBIO_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js


function getCambio (req,res){
  console.log("get /techUProyecto/cambio/:monedaOrigen/:monedaDestino/:importe");
  httpClient = requestJson.createClient(baseCambioURL);
  var monedaOrigen = req.params.monedaOrigen;//"EUR";
  var monedaDestino = req.params.monedaDestino;//"USD";
  var importe =req.params.importe;

  // console.log("URL completa "+baseCambioURL+"quotes/"+monedaOrigen+"/"+ monedaDestino+"/json?quantity="+importe+"&"  + cambioKey);

  httpClient.get("quotes/"+monedaOrigen+"/"+ monedaDestino+"/json?quantity="+importe+"&"  + cambioKey,
    function(err,resCambio,body){
      if (err){
        res.status(500).send({ "msg" : "Error"});
      } else {
        res.status(201).send(body);
      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//getCambio


function getCambioEuro (req,res){
  console.log("get /techUProyecto/cambio/:monedaOrigen");
  httpClient = requestJson.createClient(baseCambioURL);
  var monedaOrigen = req.params.monedaOrigen;//"EUR";

  // console.log("URL completa "+baseCambioURL + "full/"+monedaOrigen+"/json?"+ cambioKey);

  httpClient.get("full/"+monedaOrigen+"/json?"+ cambioKey,
    function(err,resCambio,body){
      if (err){
        res.status(500).send({ "msg" : "Error"});
      } else {
        var arrayTotal = body.result.conversion;
        var arraySalida = [];
        for ( i=0;i<arrayTotal.length ; i++)
        {
          var jsonMoneda = arrayTotal[i];
          if (jsonMoneda.to=="PEN" ||  jsonMoneda.to=="COP"||jsonMoneda.to=="ARS"||jsonMoneda.to=="UYU" ||jsonMoneda.to=="MXN"||jsonMoneda.to=="TRY"||jsonMoneda.to=="USD"||jsonMoneda.to=="EUR")
            arraySalida.push(jsonMoneda);

        }
        res.status(201).send(arraySalida);
      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//getCambioEuro


module.exports.getCambio=getCambio;
module.exports.getCambioEuro=getCambioEuro;
