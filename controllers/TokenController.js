//const requestJson = require ('request-json');
//const crypt = require('../crypt');
const jwt = require('jsonwebtoken');
//const movimientoController = require('./MovimientoController');

//const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";

//const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js
const jwtClave=process.env.JWT_CLAVE;

function validaToken(req,res){
  var token = req.headers.authorization;
  var idUsuario = req.params.idUsuario;
  // console.log("idUsuario "+idUsuario);
  var salida;
  if(!token){
    console.log("no hay token");
    salida = JSON.parse('{"msg":"Es necesario el token de autenticación","status":401 }');
  }else{
    token = token.replace('Bearer ', '');

    jwt.verify(token, jwtClave, function(err, user) {
      if (err) {
        console.log("token Error");
        salida = JSON.parse('{"msg":"Token inválido","status":401 }');
      } else {
          console.log("token OK");
        //  console.log(user);
          /*{ username: 'raul@mail.org',
            idUsuario: 18,
            iat: 1554052451,iat: Identifica la fecha de creación del token, válido para si queremos ponerle una fecha de caducidad. En formato de tiempo UNIX
            exp: 1554138851 }  exp: Identifica a la fecha de expiración del token. Podemos calcularla a partir del iat. También en formato de tiempo UNIX.
          */
          console.log(user.idUsuario+ " == "+idUsuario)
          if(user.idUsuario==idUsuario)
          {
            console.log("ACCESO OK !!!");
            salida = JSON.parse('{"msg":"acceso ok","status":200 }');
          } else {
            console.log("ACCESO PROHIBIDO !!!");
            salida = JSON.parse('{"msg":"acceso prohibido","status":403 }');
          }
      }//else
    }//jwt.verify function
    )//jwt.verify
  }
  return salida;
}//function validaToken

module.exports.validaToken=validaToken;
