const requestJson = require ('request-json');
const movimientoController = require('./MovimientoController');
const tokenController = require('./TokenController');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js

function crearCuenta(req,res){
  console.log("post /techUProyecto/cuentas/:idUsuario ");
  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  // console.log(validaToken);
  if(validaToken.status==200)
  {
    //genero nuevoIBAN
     var nuevoIBAN = generaIBAN();
      //compruebo si existe
      var queryIBAN = "q={'IBAN':'"+nuevoIBAN+"'}";
      // console.log("La consulta es "+queryIBAN);
      // console.log("url completa "+baseMLabURL+"cuentas?"+ queryIBAN +"&" + mLabAPIKey)
      httpClient = requestJson.createClient(baseMLabURL);
      //select where iban
      httpClient.get("cuentas?"+ queryIBAN +"&"  + mLabAPIKey,  //ojo con el nombre de la coleccion
        function(err,resMLab,body){
          if (err){
            res.status(500).send({ "msg" : "Error buscando IBAN"});
          } else {
            if (body[0])
            {
              // console.log("el IBAN existe");
               crearCuenta(req,res);
            }
            else {
              // console.log("el IBAN no existe");
              crearCuentaIBAN(req,res,nuevoIBAN,idUsuario);
            }
          }
        }//function(err,resMLab,body)
      )//httpClient.post
  } else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//function crearCuenta


function crearCuentaIBAN(req,res,IBAN,idUsuario){
  var nuevaCuenta = {
    "idUsuario" : parseInt(idUsuario),
    "IBAN":IBAN,
    "balance":0
  }
  // console.log("nuevaCuenta");
  // console.log(nuevaCuenta); // si concatenamos un string pondra object en la traza
  var query = "q={'idUsuario':"+idUsuario+"}";

  // console.log("La consulta es "+query);
  // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)
  httpClient = requestJson.createClient(baseMLabURL);
  // insert cuentas
  httpClient.post("cuentas?"+ query +"&"  + mLabAPIKey, nuevaCuenta,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({ "msg" : "Error insertando cuenta"});
      } else {
        console.log("Cuenta creado");
        // console.log(body);
        res.status(201).send({"msg":"cuenta "+nuevaCuenta.IBAN+" creada correctamente para el usuario "+idUsuario}); // 201 codigo para creado
      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//function crearCuentaIBAN



function generaIBAN ()
{
  //MASCARA IBAN ES99-9999-9999-99-9999999999
  var IBAN_es = zfill(Math.round(Math.random() * (99 - 1) + 1),2);
  var IBAN_banco = zfill(Math.round(Math.random() * (9999-1) + 1),4);
  var IBAN_oficina = zfill(Math.round(Math.random() * (9999-1) + 1),4);
  var IBAN_dc = zfill(Math.round(Math.random() * (99-1) + 1),2);
  var IBAN_cuenta = zfill(Math.round(Math.random() * (9999999999-1) + 1),10);

  var IBAN = "ES"+IBAN_es+"-"+IBAN_banco+"-"+IBAN_oficina+"-"+IBAN_dc+"-"+IBAN_cuenta;
  return IBAN;
  //return "ES14-9513-0641-70-0195299986";
}//generaIBAN

function zfill(numero, width) {
    // completa con 0 por la izquierda hasta width
    var length = numero.toString().length; /* Largo del número */
    var cero = "0"; /* String de cero */
    return ((cero.repeat(width - length)) + numero.toString());
}//zfill

function getCuentasUsuario (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/cuentas/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "q={'idUsuario':"+idUsuario+"}";
    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)

    var httpClient = requestJson.createClient(baseMLabURL);
    //select cuentas where idUsuario
    httpClient.get("cuentas?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          var response ={ "msg" : "Error obteniendo cuentas"};
          res.status(500);
        } else {
          if (body.length >0){
            var response = body;
          } else {
            var response = {"msg" : "usuario no encontrado"}
            res.status(404);
          }
        }
        res.send(response);
      }//function (err,resMLab, body)
    );//  httpClient.get
  }else{
    res.status(validaToken.status).send(validaToken.msg);
  }
}//getCuentasUsuario


function actualizaBalance (req, res,IBAN,importe){
  console.log("actualizaBalance");

  var query = "q={'IBAN':'"+IBAN+"'}";

  // console.log("La consulta es "+query);
  // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  //select cuenta del usuario where  IBAN
  httpClient.get("cuentas?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
        res.status(500).send({ "msg" : "Error obteniendo cuenta"});
      } else {
        if (body.length >0){
          //esta pensado con un usuario por cuenta --> si pensamos en cotitulares hay que modificar
          var balance = body[0].balance;
          // console.log("balance "+parseFloat(balance));
          // console.log("importe "+parseFloat(importe));
          var nuevoBalance = parseFloat(balance) + parseFloat(importe);
          // console.log("nuevoBalance "+parseFloat(nuevoBalance).toFixed(2));
          var putBody = '{"$set":{"balance":'+parseFloat(nuevoBalance).toFixed(2)+'}}';
          // console.log ("putBody "+putBody)
          httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body) {
                if (err){
                  res.status(500).send({ "msg" : "Error obteniendo cuenta"});
                } else {
                  console.log("Añadimos nuevo balance ");
                  res.status(201).send({ "msg" : "balance actualizado con éxito"});
                }
            })
        } else {
          res.status(403).send({ "msg" : "cuenta no encontrado"});
        }
      }
    }//function (err,resMLab, body)
  );//  httpClient.get
}//actualizaBalance




function getTotalCuentas (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/cuentas/total/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "q={'idUsuario':"+idUsuario+"}";

    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)
    var httpClient = requestJson.createClient(baseMLabURL);
    //select cuentas where idUsuario
    httpClient.get("cuentas?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          var response ={ "msg" : "Error obteniendo cuentas"};
          res.status(500);
        } else {
          if (body.length >0){
              var total =0;
              for(var i=0; i<body.length; i++){
                total =total + body[i].balance;
              }
              var response = {"total" : total}; // si pongo como response el total solo da error por ser un número
              res.status(201);
          } else {
            var response = {"msg" : "usuario no encontrado"}
            res.status(404);
          }
        }
        res.send(response);
      }//function (err,resMLab, body)
    );//  httpClient.get
  }else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//getTotalCuentas

function getMovimientosUsuario (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/movimientos/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "q={'idUsuario':"+idUsuario+"}";
    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)

    var httpClient = requestJson.createClient(baseMLabURL);
    //select cuentas where idUsuario
    httpClient.get("cuentas?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          var response ={ "msg" : "Error obteniendo cuentas"};
          res.status(500).send(response);
        } else {
          if (body.length >0){
            var response = body;
            //q={ "IBAN": {"$in": [ "ES23-5252-8512-11-2180012771", "ES60-6401-4681-92-9805401625" ]}}
            var qMovimientos = "q={ 'IBAN': {'$in': [ '";
            for(var i=0; i<body.length; i++){
              qMovimientos = qMovimientos + body[i].IBAN;
              if (i < body.length-1)
              {
                qMovimientos = qMovimientos +"','"
              }
            }//for
            qMovimientos = qMovimientos + "' ]}}"
            console.log("qMovimientos "+qMovimientos);
            movimientoController.getMovimientosUsuario(req,res,qMovimientos);
          } else {
            var response = {"msg" : "usuario no encontrado"}
            res.status(404).send(response);
          }
        }
        //res.send(response);
      }//function (err,resMLab, body)
    );//  httpClient.get
  } else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//getMovimientosUsuario



function getBalanceMesUsuario (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/balance/:idUsuario/:mes");
  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "q={'idUsuario':"+idUsuario+"}";
    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"cuentas?"+ query +"&" + mLabAPIKey)

    var httpClient = requestJson.createClient(baseMLabURL);
    //select cuentas where idUsuario
    httpClient.get("cuentas?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          var response ={ "msg" : "Error obteniendo cuentas"};
          res.status(500).send(response);
        } else {
          if (body.length >0){
            var response = body;
            var arrayCuentas= [] ;
            for(var i=0; i<body.length; i++){
              arrayCuentas[i]= body[i].IBAN;
            }
            // console.log("qCuentas "+arrayCuentas);
            movimientoController.getBalanceMesUsuario(req,res,arrayCuentas);
          } else {
            var response = {"msg" : "usuario no encontrado"}
            res.status(404).send(response);
          }
        }
      }//function (err,resMLab, body)
    );//  httpClient.get
  }else{
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//getBalanceMesUsuario


function deleteCuenta(req,res){
    console.log("delete /techUProyecto/cuentas/:idUsuario/:IBAN");
  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  // console.log(validaToken);
  if(validaToken.status==200)
  {

    var IBAN = req.params.IBAN;
    //valido que el balance es 0
    var queryIBAN = "q={'IBAN':'"+IBAN+"'}";
    // console.log("La consulta es "+queryIBAN);
    // console.log("url completa "+baseMLabURL+"cuentas?"+ queryIBAN +"&" + mLabAPIKey)
    httpClient = requestJson.createClient(baseMLabURL);
    //select where iban
    httpClient.get("cuentas?"+ queryIBAN +"&"  + mLabAPIKey,  //ojo con el nombre de la coleccion
      function(err,resMLab,body){
        if (err){
          res.status(500).send({ "msg" : "Error buscando IBAN"});
        } else {
          console.log("valido si existe IBAN "+IBAN);
          if (body[0])
          {
            console.log("el IBAN existe");
            if (body[0].balance != 0)
            {
              res.status(303).send({ "msg" : "cuenta "+IBAN+" con balance distinto de 0"});
            }else{
              eliminarCuenta(res,req);
            }
          }
          else {
            console.log("el IBAN no existe");
            res.status(404).send({ "msg" : "El IBAN "+IBAN+" no existe"});
          }
        }
      }//function(err,resMLab,body)
    )//httpClient.post
  } else {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//function deleteCuenta

function eliminarCuenta(res,req)
{
  var IBAN = req.params.IBAN;

  var queryIBAN = "q={'IBAN':'"+IBAN+"'}";
  // console.log("La consulta es "+queryIBAN);
  // console.log("url completa "+baseMLabURL+"cuentas?"+ queryIBAN +"&" + mLabAPIKey)

  httpClient = requestJson.createClient(baseMLabURL);
  var deleteCuenta  = [];
  httpClient.put("cuentas?"+ queryIBAN +"&"  + mLabAPIKey, deleteCuenta, //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({ "msg" : "Error buscando IBAN"});
      } else {
        if (body.removed == 0)
        {
          res.status(401).send({"msg" : "IBAN no encontrado" })
        }else {
          movimientoController.eliminarMovimientos(res,req);
        }
      }
    })
}//function eliminarCuenta

module.exports.actualizaBalance = actualizaBalance;
module.exports.crearCuenta = crearCuenta;
module.exports.getCuentasUsuario = getCuentasUsuario;
module.exports.getTotalCuentas = getTotalCuentas;
module.exports.getMovimientosUsuario =getMovimientosUsuario;
module.exports.getBalanceMesUsuario =getBalanceMesUsuario;
module.exports.deleteCuenta=deleteCuenta;
