const requestJson = require ('request-json');
const crypt = require('../crypt');
const jwt = require('jsonwebtoken');
const tokenController = require('./TokenController');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js
const jwtClave=process.env.JWT_CLAVE;

function login(req, res){
  console.log("----------------------");
  console.log("post/techUProyecto/login");

  //recuperamos del body el usuario.
  // console.log(req.body.email);
  // console.log(req.body.password);
  var existe = false;

  var email = req.body.email;
  var pass = req.body.password;

  var query = 'q={"email":"' + email + '"}';
  // console.log("consulta " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
        if(err) {
          var response = {"msg" : "Error obteniendo usuario"}
          res.status(500);
        } else {
          if (body.length > 0){
            var passBBDD = body[0].password;
            // Usuario recuperado comparamos las contraseñas
            var id = body[0].idUsuario;

            if (crypt.checkPassword(pass, passBBDD)){
              console.log("contraseña ok");
              //Añadir logged = true en la base de datos.
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err, resMLab, body) {
                    console.log("Añadimos propiedad logged true ");

                    /*El formato de un JWT está compuesto por 3 cadenas de caracteres separadas simplemente por un punto '.'
                    Header:La primera parte es la cabecera del token, que a su vez tiene otras dos partes, el tipo, en este caso un JWT y la codificación utilizada.
                          Comúnmente es el algoritmo HMAC SHA256*
                          {"typ": "JWT",
                          "alg": "HS256"}
                    Payload:Está compuesto por los llamados JWT Claims donde irán colocados la atributos que definen nuestro token
                    Signature:Esta firma está formada por los anteriores componentes (Header y Payload) cifrados en Base64 con una clave secreta (un secreto almacenada en nuestro servidor).
                        Así sirve de Hash para comprobar que todo está bien.*/

                    //generamos token
                    var tokenData = {
                      username: email,
                      idUsuario: id
                    }

                    var token = jwt.sign(tokenData,jwtClave, {
                       expiresIn: 60 * 60 * 24 // expires in 24 hours
                    })
                    /*opciones del ultimo parametro
                    issuer — Software organization who issues the token.
                    subject — Intended user of the token.
                    audience — Basically identity of the intended recipient of the token..
                    expiresIn — Expiration time after which the token will be invalid. --> solo estamos usando este
                    algorithm — Encryption algorithm to be used to protect the token.
                    */

                    var response = { "msg" : "Usuario logado con éxito", "idUsuario" : id,"token":token}
                    res.send(response);
                })
            }
            else {
              res.status(401);
              res.send({"msg" : "usuario / contraseña incorrecto"});
            }
          }
          else {
            res.status(404);
            res.send({"msg" : "usuario / contraseña incorrecto"});
          }
        }
     }
  )
}//login

function logout(req, res){
  console.log("post/techUProyecto/logout");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  // console.log(validaToken);
  if(validaToken.status==200)
  {
     var query = 'q={"idUsuario":' + idUsuario + '}';
    //  console.log("consulta " + query);

     var httpClient = requestJson.createClient(baseMLabURL);
     httpClient.get("usuarios?" + query + "&" + mLabAPIKey,
       function(err, resMLab, body) {
          if(err) {
            res.status(500);
            res.send({"msg" : "error recuperando ususario"});
          } else {
            if (body.length > 0 &&  body[0].logged ){

                var putBody = '{"$unset":{"logged":""}}'
                httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(err, resMLab, body) {
                      console.log("Quitamos propiedad true ");
                      //eliminamos el token de la sesion y las cabeceras
                      req.headers.authorization=null;
                      res.status(200);
                      res.send({"msg" : "usuario deslogado satisfactoriamente"});
                  })
            }
            else {
              res.status(404);
              res.send({"msg" : "usuario no encontrado"});
            }
          }
       }
    )//httpClient
  }else{
      console.log("error token ");
      res.status(validaToken.status);
      res.send({"msg" : validaToken.msg});
  }
} //function logout(req, res){


module.exports.login = login;
module.exports.logout = logout;
