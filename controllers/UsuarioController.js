const requestJson = require ('request-json');
const generator = require('generate-password');
const crypt = require('../crypt');
const jwt = require('jsonwebtoken');

const cuentaController = require('./CuentaController');
const emailController = require('./mailController');
const tokenController = require('./TokenController');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js
const jwtClave=process.env.JWT_CLAVE;

function getUsuario (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  //select * usuarios
  httpClient.get("usuarios?" + mLabAPIKey,
    function (err,resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.status(200).send(response);
    }//function (err,resMLab, body)
  );//  httpClient.get
}//getUsuario


function crearUsuarioId(req,res,idUsuario){
  console.log("function crearUsuarioId");
  var nuevoUsuario = {
    "idUsuario" :idUsuario,
    "nombre":req.body.nombre,
    "apellido1":req.body.apellido1,
    "apellido2":req.body.apellido2,
    "email":req.body.email,
    "password":crypt.hash(req.body.password)
  }
  // console.log(nuevoUsuario); // si concatenamos un string pondra object en la traza
  var mail = req.body.email;
  httpClient = requestJson.createClient(baseMLabURL);
  // insert usuario
  httpClient.post("usuarios?" + mLabAPIKey, nuevoUsuario,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({"msg":"Error insertando usuario"});
      } else {
        console.log("Usuario creado");
        //creamos la primera cuenta
        req.params.idUsuario = idUsuario;

        var tokenData = {
          username: mail,
          idUsuario: idUsuario
        }

        var token = jwt.sign(tokenData,jwtClave, {
           expiresIn: 60 * 60 * 24 // expires in 24 hours
        })
        // console.log("token");
        // console.log(token);
        req.headers.authorization = token;
        cuentaController.crearCuenta(req,res);

      }
    }//function(err,resMLab,body)
  )//httpClient.post
}//function crearUsuarioId


function crearUsuario(req, res)
{
  console.log("----------------------");
  console.log("POST /techUProyecto/users");

  var email = req.body.email;
  var query = "q={'email':'"+email+"'}";

  // console.log("La consulta es "+query);
  // console.log("url completa "+baseMLabURL+"usuarios?"+ query +"&" + mLabAPIKey)

  httpClient = requestJson.createClient(baseMLabURL);
  //select email para validar que no existe
  httpClient.get("usuarios?"+ query +"&"  + mLabAPIKey,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({"msg":"Error obteniendo mail"});
      } else {
        // console.log("busco mail");
        // console.log("body");
        // console.log(body);
        if (body[0])
        {
          console.log("el mail existe");
          res.status(403).send({"msg":"mail ya existe"});
        }
        else {
          console.log("el mail no existe puedo crearlo");
          calcularNuevoID(req,res);
        }
      }
    }//function(err,resMLab,body)
  )//httpClient.get
}//crearUsuario

function calcularNuevoID(req, res)
{
  console.log("function calcularNuevoID");

  var query = "s={'idUsuario':-1}&l=1";//s con -1 ordena descendiente y l selecciona solo uno
  // console.log("La consulta es "+query);
  // console.log("url completa "+baseMLabURL+"usuarios?"+ query +"&" + mLabAPIKey)

  httpClient = requestJson.createClient(baseMLabURL);
  //select max idUsuario para obtener el idUsuario para el nuevo usuario
  httpClient.get("usuarios?"+ query +"&"  + mLabAPIKey,  //ojo con el nombre de la coleccion
    function(err,resMLab,body){
      if (err){
        res.status(500).send({"msg":"Error obteniendo id maxima"});
      } else {
        var idNuevo = body[0] ? body[0].idUsuario+1 : 1;
        crearUsuarioId(req,res,idNuevo);
      }
    }//function(err,resMLab,body)
  )//httpClient.get
}//calcularNuevoID

function getUsuarioPorId (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/users/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    var query = "q={'idUsuario':"+idUsuario+"}";

    // console.log("La consulta es "+query);
    // console.log("url completa "+baseMLabURL+"usuarios?"+ query +"&" + mLabAPIKey)

    var httpClient = requestJson.createClient(baseMLabURL);
    //select usuarios where id
    httpClient.get("usuarios?"+ query +"&"+ mLabAPIKey,
      function (err,resMLab, body){
        if (err){
          console.log("error query")
          var response ={ "msg" : "Error obteniendo usuario"};
          res.status(500);
        } else {
          if (body.length >0){
            var response = body[0];
            res.status(200);
          } else {
            var response = {"msg" : "el usuario no tiene cuentas"}
            res.status(404);
          }
        }
        res.send(response);
      }//function (err,resMLab, body)
    );//  httpClient.get
  } else  {
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }

}//getUsuarioPorId

function getPassword (req, res){
  console.log("----------------------");
  console.log("GET  /techUProyecto/password/:mail");
  var email = req.params.mail;
  var query = "q={'email':'"+email+"'}";

  // console.log("La consulta es "+query);
  // console.log("url completa "+baseMLabURL+"usuarios?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  //select usuarios where mail
  httpClient.get("usuarios?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
        var response ={ "msg" : "Error obteniendo usuario"};
        res.status(500).send(response);
      } else {
        if (body.length >0){
          //genero nueva pass
           var password = generator.generate({
              length: 10,
              numbers: true
          });
          var passwordCrypt=crypt.hash(password);
          //actualizo en mongo
          var putBody = '{"$set":{"password":"'+passwordCrypt+'"}}';
          // console.log("La consulta es "+query);
          httpClient.put("usuarios?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body) {
                console.log("enviamos correo ");
                // console.log("email "+email);
                var mail = {
                    "to": email,
                    "subject": "[TechUBank] cambio de contraseña",
                    "text": "Su nueva contraseña es "+ password
                  }
                  req.body = mail;
                  emailController.envioMail(req,res);
            })
        } else {
          var response = {"msg" : "el usuario no tiene cuentas"}
          res.status(404).send(response);
        }
      }
    }//function (err,resMLab, body)
  );//  httpClient.get
}//getPassword

function modificarUsuario(req,res){
  console.log("----------------------");
  console.log("PUT  /techUProyecto/cuentas/usuario/:idUsuario");

  var idUsuario = req.params.idUsuario;
  var validaToken = tokenController.validaToken(req,res);
  if(validaToken.status==200)
  {
    if (req.body.password && req.body.password!=null && req.body.password!=""){
      //modifico con pass
      console.log("lleva pass");
      var usuarioModificado = {
        "nombre":req.body.nombre,
        "apellido1":req.body.apellido1,
        "apellido2":req.body.apellido2,
        "password":crypt.hash(req.body.password)
      }
    }else{
        // console.log("NO lleva pass");
        var usuarioModificado = {
          "nombre":req.body.nombre,
          "apellido1":req.body.apellido1,
          "apellido2":req.body.apellido2
        }
        // console.log(usuarioModificado); // si concatenamos un string pondra object en la traza
    }
        var query = "q={'idUsuario':"+idUsuario+"}";
        // console.log("query "+query);
        httpClient = requestJson.createClient(baseMLabURL);
        var putBody = '{"$set":'+JSON.stringify(usuarioModificado)+'}';
        // console.log ("putBody "+putBody)
        // console.log("url completa "+baseMLabURL+"usuarios?"+query + "&" + mLabAPIKey);
        // actualizar usuario
        httpClient.put("usuarios?"+query + "&" +mLabAPIKey, JSON.parse(putBody),  //ojo con el nombre de la coleccion
          function(err,resMLab,body){
            if (err){
              console.log(err);
              res.status(500).send({"msg":"Error modificando usuario"});
            } else {
              console.log("Usuario modificado ");
              res.status(201).send({"msg":"Usuario modificado correctamente"});
            }
          }//function(err,resMLab,body)
        )//httpClient.post

  }else{
      console.log("errro token");
      res.status(validaToken.status).send(validaToken.msg);
  }
}//function modificarUsuario



module.exports.getUsuario = getUsuario;
module.exports.crearUsuario = crearUsuario;
module.exports.getUsuarioPorId = getUsuarioPorId;
module.exports.getPassword = getPassword;
module.exports.modificarUsuario = modificarUsuario;
